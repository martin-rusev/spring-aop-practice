package spring.aop.demo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.aop.demo.configurations.DemoConfig;
import spring.aop.demo.dao.AccountDAO;

public class DemoApp {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(DemoConfig.class);

        AccountDAO accountDAO = context.getBean( "accountDAO", AccountDAO.class);

        String data = accountDAO.anotherMethod();

        System.out.println("Modified data is: " + data);

        System.out.println("Main program proceed with logic ...");

        context.close();
    }
}
