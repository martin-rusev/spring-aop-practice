package spring.aop.demo.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(1)
public class PerformApiAnalyticsAspect {

    @Before("spring.aop.demo.aspects.expressions.AopExpressions.forDaoPackageNoGetterAndSetter()")
    public void performApiAnalytics() {
        System.out.println("========== Perform API Analytics");

    }
}
