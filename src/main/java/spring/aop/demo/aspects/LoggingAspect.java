
package spring.aop.demo.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import spring.aop.demo.models.Account;

import java.util.List;

@Aspect
@Component
@Order(2)
public class LoggingAspect {

    @Before("spring.aop.demo.aspects.expressions.AopExpressions.forDaoPackageNoGetterAndSetter()")
    public void beforeAddAccount(JoinPoint joinPoint) {
        System.out.println("========== Executing @Before Advice!");

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        System.out.println("Method called is: " + signature);

        Object[] args = joinPoint.getArgs();

        for (Object object : args) {
            System.out.println(object.toString());
        }
    }

    @AfterReturning(pointcut = "spring.aop.demo.aspects.expressions.AopExpressions.forFindAccounts())",
            returning = "result")
    public void findAccountsAfterReturningAdvice(JoinPoint joinPoint, List<Account> result) {

        for (Account account : result) {
            account.setName("Changed Name");
            account.setLevel("Changed Level");
        }
    }

    @AfterThrowing(pointcut = "spring.aop.demo.aspects.expressions.AopExpressions.forFindAccounts())",
            throwing = "throwable")
    public void findAccountsAfterThrowingAdvice(JoinPoint joinPoint, Throwable throwable) {

        System.out.println("Logged: " + throwable);
    }

    @After("spring.aop.demo.aspects.expressions.AopExpressions.forFindAccounts()")
    public void findAccountsAfterAdvice(JoinPoint joinPoint) {

        System.out.println("Executing regardless if method is finished successfully or exception is thrown");
    }

    @Around("spring.aop.demo.aspects.expressions.AopExpressions.forDAOPackage()")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) {
        System.out.println("Before method call .....");

        long begin = System.currentTimeMillis();

        Object result = null;

        try {
            result = joinPoint.proceed();
        } catch (Throwable throwable) {
            System.out.println("Exception occurred!! " + throwable.getMessage());
        }

        long end = System.currentTimeMillis();

        long duration = end - begin;

        System.out.println("Another method execution in seconds: " + duration / 1000.0);

        return "Some string ......";

    }
}
