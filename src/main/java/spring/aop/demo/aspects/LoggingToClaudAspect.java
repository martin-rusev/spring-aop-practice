package spring.aop.demo.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(3)
public class LoggingToClaudAspect {

    @Before("spring.aop.demo.aspects.expressions.AopExpressions.forDaoPackageNoGetterAndSetter()")
    public void logToClaudAsync() {
        System.out.println("========== Logging to Claud in async fashion");

    }
}
