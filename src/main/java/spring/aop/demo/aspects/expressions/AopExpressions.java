package spring.aop.demo.aspects.expressions;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
public class AopExpressions {

    @Pointcut("execution(* spring.aop.demo.dao.*.*(..))")
    public void forDAOPackage() {}

    @Pointcut("execution(* spring.aop.demo.dao.*.get*(..))")
    public void getter() {}

    @Pointcut("execution(* spring.aop.demo.dao.*.set*(..))")
    public void setter() {}

    @Pointcut("forDAOPackage() && !(getter() || setter())")
    public void forDaoPackageNoGetterAndSetter() {}

    @Pointcut("execution(* spring.aop.demo.dao.AccountDAO.findAccounts(..))")
    public void forFindAccounts() {}

}
