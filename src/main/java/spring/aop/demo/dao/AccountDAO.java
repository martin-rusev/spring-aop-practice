package spring.aop.demo.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import spring.aop.demo.models.Account;

import java.util.ArrayList;
import java.util.List;

@Component
@Getter
@Setter
public class AccountDAO {
    private String name;
    private String serviceCode;

    public List<Account> findAccounts() {
        List<Account> accounts = new ArrayList<>();

        Account firstAccount = new Account("Jonah", "Mandatory");
        Account secondAccount = new Account("Rannah", "Gold");
        Account thirdAccount = new Account("Sarah", "Premium");

        accounts.add(firstAccount);
        accounts.add(secondAccount);
        accounts.add(thirdAccount);

        return accounts;
    }

    public void addAccount(Account account, boolean flag) {
        System.out.println(getClass() + ": Adding an account!");
    }

    public String anotherMethod()  {
        throw new RuntimeException();
    }
}
